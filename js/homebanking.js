//Declaración de variables
let nombreUsuario = "Super Diana";
let saldoCuenta = 3500;
let limiteExtraccion = 20000;

//Ejecución de las funciones que actualizan los valores de las variables en el HTML
cargarNombreEnPantalla();
actualizarSaldoEnPantalla();
actualizarLimiteEnPantalla();

//Validaciones
function haySaldoDisponible(monto) {
    return monto > saldoCuenta;
}

function checkLimiteExtraccion(monto) {
    return monto > limiteExtraccion;
}

function checkBilletesCien(monto) {
    return monto % 100 !== 0;
}

function checkIsNumero(monto) {
    return isNaN(monto) || monto === 0 || typeof monto !== "number";
}

//Funciones de suma y resta de valores
function incrementarSaldo(monto) {
    saldoCuenta += monto;
}

function restarSaldo(monto) {
    saldoCuenta -= monto;
}


//Funciones completadas para TP
function cambiarLimiteDeExtraccion() {

    limiteExtraccion = parseInt(prompt("Ingresa tu nuevo limite de extraccion"));
    if (checkIsNumero(limiteExtraccion)) {
        alert("Error: Ingresar números mayores a 0!");
    }
    actualizarLimiteEnPantalla();
    alert("Nuevo limite de extraccion: $" + limiteExtraccion);
}


function extraerDinero() {

    let montoRetiro = parseInt(prompt("Ingresa el monto a extraer"));
    let saldoAnterior = saldoCuenta;
    if (checkIsNumero(montoRetiro)) {
        alert("Error: Ingresar números mayores a cero!");
    } else if (haySaldoDisponible(montoRetiro, saldoCuenta)) {
        alert("Error: Saldo insuficiente!");
    } else if (checkLimiteExtraccion(montoRetiro)) {
        alert("Error: Monto ingresado mayor a tu límite de extracción!");
    } else if (checkBilletesCien(montoRetiro)) {
        alert("Error: Ingresa el monto a extraer en multiplos de 100!");
    } else {
        restarSaldo(montoRetiro);
        actualizarSaldoEnPantalla();
        alert("Monto de Extracción: $" + montoRetiro + "\n" + "Saldo anterior: $" + saldoAnterior + "\n" + "Saldo actual: $" + saldoCuenta);
    }
}


function depositarDinero() {

    let montoDeposito = parseInt(prompt("Ingresa el monto a depositar"));
    let saldoAnterior = saldoCuenta;
    if (checkIsNumero(montoDeposito)) {
        alert("Error: Debe ingresar sólo números mayores a cero!");
    }
    incrementarSaldo(montoDeposito);
    actualizarSaldoEnPantalla();
    alert("Monto de tu depósito: $" + montoDeposito + "\n" + "Saldo anterior: $" + saldoAnterior + "\n" + "Saldo actual: $" + saldoCuenta);

}

function pagarServicio() {

    const agua = 350;
    const luz = 210;
    const telefono = 425;
    const internet = 570;

    let servicioAPagar = parseInt(prompt("Ingrese el número que corresponda al servicio a pagar" + "\n" +
        "1. Agua" + "\n" +
        "2. Luz" + "\n" +
        "3. Internet" + "\n" +
        "4. Teléfono"));
    switch (servicioAPagar) {
        case 1:
            pagoServicio(agua);
            break;
        case 2:
            pagoServicio(luz);
            break;
        case 3:
            pagoServicio(internet);
            break;
        case 4:
            pagoServicio(telefono);
            break;
        default:
            alert("Servicio no existente");
            break;
    }
    actualizarSaldoEnPantalla();
}

function pagoServicio(servicioAPagar) {

    let saldoAnterior = saldoCuenta;
    if (servicioAPagar > saldoCuenta) {
        alert("Saldo insuficiente");
    } else {
        restarSaldo(servicioAPagar);
        alert("Tu pago se ejecutó exitosamente" + "\n" +
            "Saldo anterior: $" + saldoAnterior + "\n" +
            "Dinero descontado: $" + servicioAPagar + "\n" +
            "Saldo actual: $" + saldoCuenta);

    }
}

function transferirDinero() {
    let montoTransfer = parseInt(prompt("Ingresa el monto a transferir"));
    let saldoAnterior = saldoCuenta;
    if (checkIsNumero(montoTransfer)) {
        alert("Error: Debe ingresar sólo números mayores a cero!");
    }
    if (haySaldoDisponible(montoTransfer)) {
        alert("Error: Saldo Insuficiente!");
    }

    const cuentaAmiga1 = "1234567";
    const cuentaAmiga2 = "7654321";

    let cuentaAtransferir = prompt("Ingresa el numero de la cuenta a transferir (Recuerda que solo puedes transferir a tus cuentas asociadas):" + "\n" + "- Cuenta # 1234567" + "\n" + "- Cuenta # 7654321");
    if (cuentaAtransferir !== cuentaAmiga1 && cuentaAtransferir !== cuentaAmiga2) {
        alert("Error: Solo puedes transferir a tus cuentas afiliadas!");
    } else {
        restarSaldo(montoTransfer);
        actualizarSaldoEnPantalla();
        alert("Transferencia Exitosa!" + "\n" + "Cuenta destino: " + cuentaAtransferir + "\n" + "Monto transferido: $" + montoTransfer + "\n" + "Saldo Actual: $" + saldoCuenta);
    }
}

// Funcion Extra - Recarga celular
function recargaCelular() {

    const veinte = 20;
    const cincuenta = 50;
    const cien = 100;
    const quinientos = 500;

    let saldoAnterior = saldoCuenta;
    let monto;
    let montoEnLetras;
    let recarga = prompt("Carga de saldo - Linea Asociada:" + "\n" + "Movistar 11 44444390" + "\n" + "Ingresa el número que corresponda la recarga deseada:" + "\n" +
        "1. $20" + "\n" +
        "2. $50" + "\n" +
        "3. $100" + "\n" +
        "4. $500");

    switch (recarga) {

        case "1":
            monto = veinte;
            montoEnLetras = "$ 20";
            break;

        case "2":
            monto = cincuenta;
            montoEnLetras = "$ 50";
            break;

        case "3":
            monto = cien;
            montoEnLetras = "$ 100";
            break;

        case "4":
            monto = quinientos;
            montoEnLetras = "$ 500";
            break;

        default:
            alert("Opción Inválida!");
            return;
    }
    if (haySaldoDisponible(recarga)) {
        alert("Error: Saldo insuficiente!");
    }
    restarSaldo(recarga);
    actualizarSaldoEnPantalla();
    alert("Has recargado " + montoEnLetras + " a tu línea asociada" + "\n" + "Saldo anterior: $" + saldoAnterior + "\n" + "Dinero descontado: $" + monto + "\n" + "Saldo actual: $" + saldoCuenta);
}

//Funciones que actualizan el valor de las variables en el HTML
function cargarNombreEnPantalla() {
    document.getElementById("nombre").innerHTML = "Bienvenido/a " + nombreUsuario;
}

function actualizarSaldoEnPantalla() {
    document.getElementById("saldo-cuenta").innerHTML = "$" + saldoCuenta;
}

function actualizarLimiteEnPantalla() {
    document.getElementById("limite-extraccion").innerHTML = "Tu límite de extracción es: $" + limiteExtraccion;
}

function ocultarBotones() {
    let i = 0;
    do {
        document.getElementsByTagName("button").item(i).remove();
        i = document.getElementsByTagName("button").length - 1;
    } while (i >= 0);
}

function logout() {
    alert("Sesión finalizada! - Vuelva pronto!");
    window.location = "index.html";
}